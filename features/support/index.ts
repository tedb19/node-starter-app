import { When, Then } from 'cucumber';
import { expect } from 'chai';
import { say } from '../../src/greeting';

let name: string;

When('we say hello to {string}', (hello: string) => {
  name = hello;
});

Then('the system should greet us with {string}', (greet: string) => {
  expect(greet).to.eql(say(name));
});
