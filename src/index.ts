/**
 * We need to load New Relic module as the first thing our application ,
 * as it needs to bootstrap itself before the rest of your application loads
 */
import 'newrelic';
import * as Sentry from '@sentry/node';
import pino from 'pino';
import { default as KoaRouter } from 'koa-router';
import { default as Koa } from 'koa';

const logger = pino();
const app = new Koa();
const router = new KoaRouter();
const { PORT } = process.env;

// logger.info('hello world');

Sentry.init({ dsn: process.env.SENTRY_DSN });

router.get('/status', (ctx: any, next) => {
  ctx.body = { status: 'OK' };
});

app.use(router.routes());
app.use(router.allowedMethods());

export default app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
