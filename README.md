# Node Template

This project consists of a template for all projects using NodeJS in Shuttle

## Setup

To make sure you can use this template set it up as a remote in git. For example:

```sh
git remote add shuttle-navigation git@github.com:wundercar/shuttle-navigation.git
git remote add shuttle-driver-api git@github.com:wundercar/shuttle-driver-api.git
git remote add shuttle-passenger-web git@github.com:wundercar/shuttle-passenger-web.git
git remote add shuttle-passenger-api git@github.com:wundercar/shuttle-passenger-api.git
git remote add shuttle-identity git@github.com:wundercar/shuttle-identity.git
git remote add shuttle-mission-control-web git@github.com:wundercar/shuttle-mission-control-web.git
git remote add shuttle-mission-control-api git@github.com:wundercar/shuttle-mission-control-api.git
```

To update the repos based on the new changes in the template, do the following:

```sh
git push -u shuttle-navigation master:feature/update-template
git push -u shuttle-driver-api master:feature/update-template
git push -u shuttle-passenger-web master:feature/update-template
git push -u shuttle-passenger-api master:feature/update-template
git push -u shuttle-identity master:feature/update-template
git push -u shuttle-mission-control-web master:feature/update-template
git push -u shuttle-mission-control-api master:feature/update-template
```

To use this as a template for a new repo, do:

```sh
git push -u REMOTE_NAME master
```
