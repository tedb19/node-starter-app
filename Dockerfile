ARG NODE_VERSION=10

### Build Image Phase

FROM node:${NODE_VERSION}-alpine AS builder

RUN mkdir -p /opt/app
WORKDIR /opt/app

ADD package.json ./
ADD yarn.lock ./
RUN yarn

ADD . .
RUN yarn build

### Runtime Image Phase
FROM node:${NODE_VERSION}-alpine

ENV NODE_ENV=production

RUN mkdir -p /opt/app
WORKDIR /opt/app

COPY --from=builder /opt/app/package.json .
COPY --from=builder /opt/app/yarn.lock .
COPY --from=builder /opt/app/dist .

RUN yarn install --production

CMD ["yarn", "start"]
