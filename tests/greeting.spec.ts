import { say } from '../src/greeting';
import { expect } from 'chai';

describe('say', () => {
  it('is "Hello Juan" when "Juan" is passed as a parameter', () => {
    expect(say('Juan')).to.eql('Hello Juan!');
  });
});
