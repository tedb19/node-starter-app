import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';

import { default as app } from '../src';
chai.use(chaiHttp);

afterEach(() => {
  app.close();
});

describe('GET /status', () => {
  it('has http status 200', (done) => {
    chai.request(app)
      .get('/status')
      .set('Accept', 'application/json')
      .then((res) => {
        expect(res.status).to.eql(200);
        done();
      });
  });

  it('has content type application/json', (done) => {
    chai.request(app)
      .get('/status')
      .set('Accept', 'application/json')
      .then((res) => {
        expect(res.type).to.include('application/json');
        done();
      });
  });
});
