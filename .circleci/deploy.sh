#!/usr/bin/env bash

ROOT_DIR=`pwd`

source $ROOT_DIR/.circleci/utils.sh

activateServiceAccount(){
    info "Activating Google service Account"
    echo $GCLOUD_SERVICE_KEY | base64 --decode > $HOME/account.json
    # setup kubectl auth
    gcloud auth activate-service-account --key-file $HOME/account.json
    gcloud --quiet config set project ${PROJECT_ID}
    gcloud --quiet config set compute/zone ${COMPUTE_ZONE}
    gcloud --quiet container clusters get-credentials ${CLUSTER_NAME}
}

buildAndPushImage() {
    IMAGE_TAG=$(git rev-parse --short HEAD)
    IMAGE_NAME=eu.gcr.io/${PROJECT_ID}/${PROJECT_NAME}:${IMAGE_TAG}

    # Building image
    docker build  -t ${IMAGE_NAME} .
    # store image tag in file to be used in the next job.
    echo $IMAGE_TAG > tag.txt
    # push image to GCR repository
    docker push $IMAGE_NAME
}

deploy() {
    IMAGE_TAG=$(cat tag.txt)
    pushd deploy
    # should be updated
    make upgrade chart=$CHART_NAME namespace=$NAMESPACE ARGS="--set image.tag=${IMAGE_TAG}"
    popd
}

$@
